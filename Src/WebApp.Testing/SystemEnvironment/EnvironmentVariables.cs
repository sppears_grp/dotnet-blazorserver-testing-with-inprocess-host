using System;
using System.Collections.Generic;


namespace WebApp.Testing.Functional.SystemEnvironment
{
    public class EnvironmentVariables
    {
        /// <summary>
        /// Delegate used to set environment variable from .env variables 
        /// loaded from file
        /// </summary>
        private Action<string, IDictionary<string, string>> _SetEnvVarAction { get; }

        /// <summary>Set of actual environment variables configured</summary>
        private HashSet<string> _ActualEnvVars { get; }

        /// <summary>Set containing the names of required environment variables</summary>
        public HashSet<string> ExpectedEnvVars { get; }

        /// <summary>Set containing the names of expected environment variables</summary>
        public HashSet<string> ExpectedDotEnvVars { get; }


        /// <summary>
        /// Initialise expected environment variables
        /// and create a private set of actual environment variables
        /// </summary>
        /// <param name="expectedVars">
        /// Set containing the names of required environment variables
        /// </param>
        /// <param name = "setEnvVarAction">
        /// Delegate used to set an environment variable to a value
        /// of a variable read from .env file
        /// </param>
        public EnvironmentVariables(
            HashSet<string> expectedVars,
            HashSet<string> expecteDotEnvVars,
            Action<string, IDictionary<string, string>> setEnvVarAction
        )
        {
            _ActualEnvVars = new HashSet<string>();
            ExpectedDotEnvVars = expecteDotEnvVars;
            ExpectedEnvVars = expectedVars;
            _SetEnvVarAction = setEnvVarAction;

            var envVars = Environment.GetEnvironmentVariables();
            foreach (var key in envVars.Keys)
                _ActualEnvVars.Add(key as string);
        }


        /// <summary>
        /// Entrypoint that tries to load the .env file and set each required 
        /// environment variable with a value from .env file, using the delegate
        /// supplied in the constructor
        /// </summary>
        /// <param name="path">Absolute path to .env file</param>
        /// <exception cref="InvalidOperationException">
        /// Thrown when expected environment variables not present in .env file
        /// </exception>
        public void InitialiseFromEnvFile(string path)
        {
            bool AreEnvironmentVariablesSet = AreRequiredEnvironmentVariablesSet();

            // if the variables are not set and not running in a container environment
            if (!HostingEnvironment.IsRunningInContainer() && !AreEnvironmentVariablesSet)
            {
                DotEnvFile envFile = new DotEnvFile(path);
                envFile.EnsureInitialised(ExpectedDotEnvVars);

                foreach (var envVar in ExpectedEnvVars)
                    _SetEnvVarAction(envVar, envFile.EnvVars);
            }
            else
            {
                if (!AreEnvironmentVariablesSet)
                    throw new InvalidOperationException("Expected environment variables have not been set");
            }
        }


        /// <summary>Determine if expected environment variables are set</summary>
        /// <returns>True if expected variables are set, otherwise false</returns>
        /// <remarks>
        /// If the expected environment variables are a subset of actual environment 
        /// variables then valid
        /// </remarks>
        private bool AreRequiredEnvironmentVariablesSet()
        {
            return ExpectedEnvVars.IsSubsetOf(_ActualEnvVars);
        }
    }
}