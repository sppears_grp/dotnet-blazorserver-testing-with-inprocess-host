namespace WebApp.Testing.Functional.SystemEnvironment
{
    public class EnvFileParseOptions
    {
        public bool TrimWhitespace { get; }
        public bool IsEmbeddedHashComment { get; }
        public bool UnescapeQuotedValues { get; }
        public bool ClobberExistingVars { get; }
        public bool ParseVariables { get; }

        public EnvFileParseOptions(
            bool trimWhitespace = true,
            bool isEmbeddedHashComment = true,
            bool unescapeQuotedValues = true,
            bool clobberExistingVars = true,
            bool parseVariables = true
        )
        {
            TrimWhitespace = trimWhitespace;
            IsEmbeddedHashComment = isEmbeddedHashComment;
            UnescapeQuotedValues = unescapeQuotedValues;
            ClobberExistingVars = clobberExistingVars;
            ParseVariables = parseVariables;
        }
    }
}
