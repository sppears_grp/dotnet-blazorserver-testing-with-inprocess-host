using System;


namespace WebApp.Testing.Functional.SystemEnvironment
{
    /// <summary>
    /// Determines if running in a container based upon
    /// the presence of DOTNET_RUNNING_IN_CONTAINER
    /// environment variable set to true
    /// </summary>
    public static class HostingEnvironment
    {
        private const string DotNetRunningInContainerKey = "DOTNET_RUNNING_IN_CONTAINER";


        /// <summary>
        /// Determine if .Net is running in a container
        /// </summary>
        /// <returns>True if running in a container, otherwise false</returns>
        /// <remarks>
        /// Inspects value of DOTNET_RUNNING_IN_CONTAINER for 'true' value
        /// </remarks>
        public static bool IsRunningInContainer()
        {
            var envVar = Environment.GetEnvironmentVariable(DotNetRunningInContainerKey);

            if (string.IsNullOrEmpty(envVar))
                return false;

            return envVar.Equals("true");
        }

    }
}