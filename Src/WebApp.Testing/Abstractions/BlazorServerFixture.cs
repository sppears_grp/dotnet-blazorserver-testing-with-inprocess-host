using System;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;
using Xunit;


namespace WebApp.Testing.Functional.Abstractions
{
    /// <summary>
    /// Blazor Server functional test fixture
    /// </summary>
    /// <typeparam name="THost">
    /// In-process host, derived from <see cref="TestHost"/>
    /// </typeparam>
    /// <typeparam name="TPage">
    /// Blazor Server Page <see cref="BlazorServerPage"/>
    /// </typeparam>
    /// <remarks>
    /// A future release of Xunit will provide <see cref="IAsyncDisposable"/> 
    /// interface for test fixtures. Until then, <typeparamref name="THost"/>
    /// is disposed using Xunit interface <see cref="IAsyncLifetime"/>
    /// </remarks>
    public class BlazorServerFixture<THost, TPage> : IAsyncLifetime
        where THost : TestHost, new()
        where TPage : BlazorServerPage, new()
    {
        #region Properties
        /// <summary>In-process test host</summary>
        public THost Host { get; set; }
        #endregion

        #region Fixture Setup

        /// <summary>
        /// Construct the in-process host
        /// </summary>
        public BlazorServerFixture()
        {
            Host = new THost();
        }
        #endregion

        #region IAsyncLifetime, Controls host lifetime
        /// <summary>
        /// Stop the in-process test host
        /// </summary>
        public virtual async Task DisposeAsync()
        {
            await Host?.DisposeAsync();
        }

        /// <summary>
        /// Start the in-process test host
        /// </summary>
        public virtual async Task InitializeAsync()
        {
            await Host?.InitializeAsync();
        }
        #endregion

        /// <summary>
        /// Create a Blazor Server Page using the supplied factory method
        /// </summary>
        /// <param name="factory">
        /// Method that creates the <see cref="IServiceCollection"/> containing
        /// service dependencies for the Page Under Test (PUT)
        /// </param>
        /// <returns>
        /// Blazor Server page initialised with services constructed via factory method
        /// </returns>
        /// <remarks>It is the reponsibility of the caller to dispose the page</remarks>
        public BlazorServerPage CreatePageFromServices(Func<IServiceCollection> factory)
        {
            var page = new TPage();
            page.InitialiseServices(factory());

            return page;
        }
    }
}