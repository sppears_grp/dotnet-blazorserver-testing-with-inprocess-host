using System;
using System.Collections.Generic;

using WebApp.Testing.Functional.SystemEnvironment;


namespace WebApp.Testing.Functional.Abstractions
{
    public abstract class SystemEnvironmentFactory
    {
        protected abstract EnvironmentContext EnvironmentContext { get; set; }


        /// <summary>
        /// Try and load environment variables from .env file
        /// </summary>
        /// <param name="context">
        /// The system environment context, <see cref="EnvironmentContext"/>
        /// </param>
        /// <remarks>
        /// The environment variables are only loaded if:
        /// 1. Not running within a container, i.e. DOTNET_RUNNING_IN_CONTAINER is unset or != 'true'
        /// 2. Environment variables have not been previously set
        /// </remarks>
        public virtual void LoadEnvironmentVariables()
        {
            var setEnvVarsHelper = new EnvironmentVariables(
                           EnvironmentContext.EnvironmentVariables,
                           EnvironmentContext.DotEnvVariables,
                           EnvironmentContext.MappingDelegate
                       );

            setEnvVarsHelper.InitialiseFromEnvFile(EnvironmentContext.DotEnvFilePath);
        }
    }
}
