using System;

using Bunit;
using Microsoft.Extensions.DependencyInjection;


namespace WebApp.Testing.Functional.Abstractions
{
    /// <summary> 
    /// Test context representation for a Blazor Server Page
    /// via Bunit. This class allows service dependencies to be
    /// registered. 
    /// </summary>
    public class BlazorServerPage : IDisposable
    {
        /// <summary>
        /// BUnit test context for blazor server page
        /// </summary>
        public TestContext PageContext { get; private set; }



        #region Constructor
        /// <summary>
        /// Initialise Blazor server page test context
        /// </summary>
        public BlazorServerPage()
        {
            PageContext = new TestContext();
        }
        #endregion


        /// <summary>
        /// Initialise services in wrapped Bunit test context from <see cref="IServiceCollection"/>
        /// </summary>
        /// <param name="services"><see cref="IServiceCollection"/> of dependencies</param>
        public virtual void InitialiseServices(IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            foreach (var service in services)
                PageContext.Services.Add(service);
        }


        #region Dispose
        private bool _disposed = false;

        /// <summary>
        /// Dispose the wrapped Bunit context
        /// </summary>
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                PageContext.Dispose();
            }

            _disposed = true;
        }
        #endregion
    }
}