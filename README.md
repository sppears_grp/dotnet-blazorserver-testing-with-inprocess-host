# Testing Blazor Server Pages Using In-Process Host and Bunit

This is a small class library that provides some key abstractions used in an implementation of integration/functional testing for a Blazor Server Page. The approach uses an in-process generic host and [Bunit](https://bunit.egilhansen.com/index.html). Each test spawns a BUnit test context that is initialised with required service dependencies obtained from the generic test host. Feel free to improve, adapt and use in projects.

I have used it in situations where the in-memory test server provided by [WebApplicationFactory](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.mvc.testing.webapplicationfactory-1?view=aspnetcore-5.0) is insufficient due to the lack of use of network sockets.

An example can be found in a recent [project](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/tree/feat/CI), [here](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/tree/feat/CI/Tests/FunctionalTests/WebApp.FunctionalTests).

A class diagram generated using [plantuml](https://plantuml.com/) is illustrated below.

![class](./class-diagram.png)

The key abstractions are:
- *ConfigMetaData*: Small helper class that represents meata data for the application settings file used, e.g. base path, filename and absolute path.
- *TestHost*: Abstract class that represents an in-process Kestrel generic host for testing.
- *AutofacTestHost*: Generic host that provides a default configuration for using Autofac IOC. Accepts a generic type parameter for the *Startup* class used to bootstrap the host.
- *SystemEnvironmentFactory*: Abstract class for initialising variables from *.env* file or validating that the expected system environment variables have been set, if running in a container. 
- *EnvironmentContext*: Meta data provided that relates to the expected configuration variables to load from .env file or system environment. Also accepts a function to provide a mapping from a .env file variable to a system environment variable.
- *BlazorServerFixture*: Generic class that starts/stops a test host and provides a factory method to create a *BlazorServerPage* Bunit test context. The host and page are specified using generic types. This can be used to start and stop the host before running a collection of xUnit tests.
- *BlazorServerPage*: Wrapper class for a Bunit [TestContext](https://bunit.egilhansen.com/api/Bunit.TestContext.html). Provides a method to initialise sevices from an *IServiceCollection* 
- *BlazorServerTest*: xUnit Test base class that provides a default test setup and teardown for testing a Blazor Server Page. It creates a *BlazorServerPage* Bunit test context before a test is run. The test context is injected with required service dependencies from the host via the factory method provided by the *BlazorServerFixture* instance.  After the test has run the *BlazorServerPage* test context is disposed.


## TestHost

An example subclass can be found [here](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/blob/feat/CI/Tests/FunctionalTests/WebApp.FunctionalTests/Servers/WebAppTestHost.cs). 

Configuration meta data is automatically stored depending upon the presence of the *ASPNETCORE_ENVIRONMENT* variable. By default the host will reference the applications settings file as *appsettings.{environment}.json*
If the *ASPNETCORE_ENVIRONMENT* is not set then uses default settings file, *appsettings.Local.json*. Settings files are referenced according to the location of the executing assembly. Meta data such as the configuration basepath and filename is logged and represented using the *ConfigMetaData* class.

``` C#
 public static ConfigMetaData GetConfigMetaData(string defaultSettingsFile = "appsettings.Local.json")
        {
            var envVar = System.Environment.GetEnvironmentVariable(AspNetCoreEnvironment);
            var ConfigFileName = defaultSettingsFile;

            if (envVar != null)
                ConfigFileName = $"appsettings.{envVar}.json";

            var ConfigBasePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var SettingsFile = $"{ConfigBasePath}/{ConfigFileName}";

            return new ConfigMetaData(SettingsFile, ConfigBasePath, ConfigFileName);
        }
```

Subclasses should override:

1. LoadSystemEnvironment
2. BuildServer

Implement the *LoadSystemEnvironment* method to initialise system environment variables required for host service(s) configuration, e.g. secrets etc. For example, it may be that the host is running locally and requires initialisation from .env file. Alternatively, this could be via environment variables if running remotely, e.g a CI server. The *SystemEnvironmentFactory* provides an abstraction for this and is best explained by [example](https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/blob/feat/CI/Tests/Utils/WebApp.Testing.Utils/TestEnvironment/WebAppSystemEnvironmentFactory.cs).

Override the *BuildServer* method to customise the services and configuration for the generic host. A default generic host that uses Autofac IOC is provided by the *AutofacTestHost* class.

## AutofacTestHost

A *TestHost* subclass that provides a default implementation for generic hosts using Autofac IOC. Creates an optional lifetime/base scope tagged as "transaction", if required. The default Autofac generic host configuration is listed below:

```C#
protected override IHost BuildServer(HostBuilder builder)
{
    return new HostBuilder()
        .UseServiceProviderFactory(new AutofacServiceProviderFactory())
        .ConfigureLogging(logging =>
        {
            logging.ClearProviders();
            logging.AddConsole();
            logging.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Information);
        })
        .ConfigureWebHost(webBuilder =>
        {
            webBuilder.ConfigureAppConfiguration((context, cb) =>
            {
                cb.AddJsonFile(ConfigMetaData.SettingsFile, optional: false)
                .AddEnvironmentVariables();
            })
            .ConfigureServices(services =>
            {
                services.AddHttpClient();
            })
            .UseStartup<TStartup>()
            .UseKestrel()
            .UseUrls("http://127.0.0.1:0");
        }).Build();
}
```

# Issues

Blazor Server uses [RemoteNavigationManager](https://github.com/dotnet/aspnetcore/tree/47a299160522100cdfa716926bbc8f6318c7a2da/src/Components/Server/src/Circuits) as a subclassed [NavigationManager]() instance. It also  implements [IHostEnvironmentNavigationManager](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.components.navigationmanager?view=aspnetcore-5.0) interface to initialise *baseUri* and *uri*. If the BlazorServerPages, RazorPages and Logging services are added to the Page Under Test (PUT), in addition to an *IConfiguration* from the self host, there is an error thrown that *RemoteNavigationManager* failed to initialise. Unfortunately, *RemoteNavigationManager* is an internal class, so it cannot be injected as a service dependency for the Page Under Test (PUT). The author tried resolving it from the test host and casting it as *RemoteNavigationManager*. Furthermore, also tried resolving a *NavigationManager* instance from self host and adding it to the service dependencies of the Page Under Test (PUT). The initialisation exception still occurs. For this reason, the author injected a mocked *NavigationManager* instance into the Page Under Test (PUT). Does anyone know how to resolve an instance of *RemoteNavogationManager* for injection within a BUnit test context?


```C#
using Microsoft.AspNetCore.Components;


namespace WebApp.Testing.Mocks.Blazor
{
    public sealed class MockNavigationManager : NavigationManager
    {
        public MockNavigationManager(string baseUri, string uri) : base() =>
          this.Initialize(baseUri, uri);

        protected override void NavigateToCore(string uri, bool forceLoad) =>
          this.WasNavigateInvoked = true;

        public bool WasNavigateInvoked { get; private set; }
    }
}
```

# Future Improvements/Ideas
- Add a subclass of *TestHost* to provide a default implementation of Microsoft IOC.
- Add an additional host abstraction for using [WebApplicationFactory]([WebApplicationFactory](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.mvc.testing.webapplicationfactory-1?view=aspnetcore-5.0))
- Add hooks / abstractions to allow subclasses to specify rollback actions to perform after each test is run. Example actions include rolling back the state of external database tables, repositories etc.